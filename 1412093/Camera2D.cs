﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093
{
    class Camera2D : AbstractCamera {
        private float _scaleX;
        private float _scaleY;
        private float _transX;
        private float _transY;
        private float _rotZ;

        public Camera2D(float scaleX, float scaleY, float transX, float transY, float rotZ)
        {
            _scaleX = scaleX;
            _scaleY = scaleY;
            _transX = transX;
            _transY = transY;
            _rotZ = rotZ;
        }

        public void Move(float x, float y)
        {
            this._transX += x;
            this._transY += y;
        }

        public void CenterOn(float x, float y) {
            this._transX = x;
            this._transY = y;
        }

        public void ZoomOnDemand(Vector3 point, float scale)
        {
            this._view = Matrix.CreateScale(scale);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (_pathUpdater != null)
            {
                _transX += _pathUpdater.Position.X;
                _transY += _pathUpdater.Position.Y;
            }

            this.World = Matrix.CreateScale(_scaleX, _scaleY, 1f)
                         * Matrix.CreateRotationZ(_rotZ)
                         * Matrix.CreateTranslation(_transX, _transY, 0f);
        }
    }
}