﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1412093.Maze {
    class MazeGenerator
    {
        private Cell[,] _maze;
        private Cell _head;
        private Cell _entrance;
        private Cell _exit;
        private int _exitBorder;
        private Random _rng;
        private int _rows;
        private int _columns;

        private static MazeGenerator _instance;

        public MazeGenerator()
        {
            _head = new Cell();
            Exit = new Cell();
            Entrance = new Cell();
            RNG = new Random();
            _exitBorder = 0;
            Rows = 0;
            Columns = 0;
        }

        public int Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }

        public int Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        public static MazeGenerator Instance
        {
            get {
                if (_instance == null)
                    _instance = new MazeGenerator();
                return _instance;
            }
        }

        public Cell[,] Cells
        {
            get { return _maze; }
            set { _maze = value; }
        }

        public Random RNG
        {
            get { return _rng; }
            set { _rng = value; }
        }

        public Cell Exit
        {
            get { return _exit; }
            set { _exit = value; }
        }

        public Cell Entrance
        {
            get { return _entrance; }
            set { _entrance = value; }
        }

        private int[] randomizeDirections() {
            int[] randoms = new int[4];
            for (int i = 0; i < 4; i++)
                randoms[i] = i + 1;
            randoms = randoms.OrderBy(x => RNG.Next()).ToArray();
            return randoms;
        }

        private void randomizeEntrance()
        {
            // To maximize the amount of time the player has to spend in the maze,
            // I'm gonna put the entrance on the opposite border to the exit
            switch (_exitBorder) {
                case 1:
                    Entrance.Y = Columns - 1;
                    for (int i = 0; i < Rows; i++)
                    {
                        if (Cells[i, Columns - 2].State == 0)
                            Entrance.X = i;
                    }
                    break;
                case 2:
                    Entrance.X = Rows - 1;
                    for (int i = 0; i < Columns; i++) {
                        if (Cells[Rows - 2, i].State == 0)
                            Entrance.Y = i;
                    }
                    break;
                case 3:
                    Entrance.Y = 0;
                    for (int i = 0; i < Rows; i++) {
                        if (Cells[i, 1].State == 0)
                            Entrance.X = i;
                    }
                    break;
                case 4:
                    Entrance.X = 0;
                    for (int i = 0; i < Columns; i++) {
                        if (Cells[1, i].State == 0)
                            Entrance.Y = i;
                    }
                    break;
                default:
                    Entrance.Y = Rows - 1;
                    for (int i = 0; i < Columns; i++) {
                        if (Cells[i, Rows - 2].State == 0)
                            Entrance.X = i;
                    }
                    break;
                    break;
            }
        }

        private void randomizeExit()
        {
            // Determine which border the exit is on
            var borders = new[] {1, 2, 3, 4}; // left, top, right, bottom
            _exitBorder = borders[RNG.Next(borders.Length)];
            // Randomize the other coordinate
            switch (_exitBorder)
            {
                case 1:
                    _head.Y = 1;
                    while (_head.X % 2 == 0) _head.X = RNG.Next(1, Rows - 2);
                    Exit.Y = 0;
                    Exit.X = _head.X;
                    break;
                case 2:
                    _head.X = 1;
                    while (_head.Y % 2 == 0) _head.Y = RNG.Next(1, Columns - 2);
                    Exit.X = 0;
                    Exit.Y = _head.Y;
                    break;
                case 3:
                    _head.Y = Columns - 2;
                    while (_head.X % 2 == 0) _head.X = RNG.Next(1, Rows - 2);
                    Exit.Y = Columns - 1;
                    Exit.X = _head.X;
                    break;
                case 4:
                    _head.X = Rows - 2;
                    while (_head.Y % 2 == 0) _head.Y = RNG.Next(1, Columns - 2);
                    Exit.X = Rows - 1;
                    Exit.Y = _head.Y;
                    break;
                default:
                    _head.X = 1;
                    while (_head.Y % 2 == 0) _head.Y = RNG.Next(0, Columns - 1);
                    Exit.X = 0;
                    Exit.Y = _head.Y;
                    break;
            }
        }

        public void outputToConsole()
        {
            String line = "";
            for (int i = 0; i < Rows; i++) {
                for (int j = 0; j < Columns; j++)
                {
                    line = line + Cells[i, j].State.ToString();
                }
                Debug.WriteLine(line);
                line = "";
            }
            Debug.WriteLine("Entrance: " + Entrance.X.ToString() + ", " + Entrance.Y.ToString());
            Debug.WriteLine("Exit: " + Exit.X.ToString() + ", " + Exit.Y.ToString());
            Debug.WriteLine("DFS start: " + _head.X.ToString() + ", " + _head.Y.ToString());
            Debug.WriteLine("Spawn info-------");
            for (int i = 0; i < Rows; i++) {
                for (int j = 0; j < Columns; j++) {
                    line = line + Cells[i, j].Content.ToString();
                }
                Debug.WriteLine(line);
                line = "";
            }
        }

        private void randomizeSpawn(int x, int y)
        {
            // 0 = Nothing, 1 = Monster, 2 = Treasure
            List<KeyValuePair<int, double>> elements = new List<KeyValuePair<int, double>>() {
                new KeyValuePair<int, double>(1, 0.03),
                new KeyValuePair<int, double>(2, 0.22),
                new KeyValuePair<int, double>(0, 0.75)
            };
           
            double diceRoll = RNG.NextDouble();
            int spawn = 0;
            double cumulative = 0.0;
            for (int i = 0; i < elements.Count; i++) {
                cumulative += elements[i].Value;
                if (diceRoll < cumulative) {
                    spawn = elements[i].Key;
                    break;
                }
            }
            if (_maze[x, y].Content == 0) _maze[x, y].Content = (byte)spawn;
            //switch (spawn)
            //{
            //    case 0:
            //        _maze[x, y].Content = 0;
            //        break;
            //    case 1:
            //        byte[] monsters = new byte[] {0, 1, 2};
            //        _maze[x, y].Content = (byte)rng.Next(monsters.Length);
            //        break;
            //    case 2:
            //        byte[] treasureTypes = new byte[] { 0, 1, 2, 3 };
            //        _maze[x, y].Content = (byte)rng.Next(treasureTypes.Length);
            //        break;
            //    default:
            //        _maze[x, y].Content = 0;
            //        break;
            //}
        }

        // Only works for odd number sized mazes, to ensure the maze is surrounded by walls
        public void generateMaze()
        {
            Cells = new Cell[Rows, Columns];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Cells[i, j] = new Cell();
                    Cells[i, j].X = i;
                    Cells[i, j].Y = j;
                    Cells[i, j].State = 1;
                }
            }
            randomizeExit();
            dfs(_head.X, _head.Y);
            randomizeEntrance();
            Cells[_head.X, _head.Y].State = 0;
            Cells[Exit.X, Exit.Y].State = 0;
            Cells[Entrance.X, Entrance.Y].State = 0;
        }

        private void dfs(int r, int c) {
            int[] directions = randomizeDirections();
            for (int i = 0; i < directions.Length; i++) {

                switch (directions[i]) {
                    case 1:
                        if (r - 2 <= 0)
                            continue;
                        if (Cells[r - 2, c].State != 0) {
                            Cells[r - 2, c].State = 0;
                            Cells[r - 1, c].State = 0;
                            randomizeSpawn(r - 2, c);
                            dfs(r - 2, c);
                        }
                        break;
                    case 2:
                        if (c + 2 >= Columns - 1)
                            continue;
                        if (Cells[r, c + 2].State != 0) {
                            Cells[r, c + 2].State = 0;
                            Cells[r, c + 1].State = 0;
                            randomizeSpawn(r, c + 2);
                            dfs(r, c + 2);
                        }
                        break;
                    case 3:
                        if (r + 2 >= Rows - 1)
                            continue;
                        if (Cells[r + 2, c].State != 0) {
                            Cells[r + 2, c].State = 0;
                            Cells[r + 1, c].State = 0;
                            randomizeSpawn(r + 2, c);
                            dfs(r + 2, c);
                        }
                        break;
                    case 4:
                        if (c - 2 <= 0)
                            continue;
                        if (Cells[r, c - 2].State != 0) {
                            Cells[r, c - 2].State = 0;
                            Cells[r, c - 1].State = 0;
                            randomizeSpawn(r, c - 2);
                            dfs(r, c - 2);
                        }
                        break;
                }
            }

        }
    }
}
