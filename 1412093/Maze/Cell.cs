﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1412093.Maze {
    public class Cell
    {
        private byte _state;
        private byte _content;
        private int _x;
        private int _y;
        private bool _hasSpawn = false;

        public byte State
        {
            get { return _state; }
            set { _state = value; }
        }

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public byte Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public bool HasSpawn
        {
            get { return _hasSpawn; }
            set { _hasSpawn = value; }
        }
    }
}
