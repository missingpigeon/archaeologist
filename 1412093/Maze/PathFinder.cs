﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093.Maze {
    class Pathfinder
    {
        private Vector2 _goal;
        private PathfinderNode endNode;
        private PathfinderNode[,] nodes;
        private static Pathfinder _instance;

        public static Pathfinder Instance {
            get {
                if (_instance == null)
                    _instance = new Pathfinder();
                return _instance;
            }
        }

        public Pathfinder()
        {
            nodes = new PathfinderNode[MazeGenerator.Instance.Rows, MazeGenerator.Instance.Columns];
            for (int i = 0; i < MazeGenerator.Instance.Rows - 1; i++)
            {
                for (int j = 0; j < MazeGenerator.Instance.Columns - 1; j++)
                {
                    this.nodes[i, j] = new PathfinderNode(new Vector2(i, j));
                    this.nodes[i, j].IsWalkable = (MazeGenerator.Instance.Cells[i, j].State != 1);
                    this.nodes[i, j].CalculateCosts();
                }
            }
            _goal = new Vector2(MazeGenerator.Instance.Exit.X, MazeGenerator.Instance.Exit.Y);
            endNode = new PathfinderNode(_goal);
            endNode.CalculateCosts();
        }


        private bool Search(PathfinderNode currentNode) {
            currentNode.State = PathfinderNode.NodeState.Closed;
            List<PathfinderNode> nextNodes = GetAdjacentWalkableNodes(currentNode);
            nextNodes.Sort((node1, node2) => node1.F.CompareTo(node2.F));
            foreach (var nextNode in nextNodes) {
                if (nextNode.Location == this._goal) {
                    return true;
                }
                else {
                    if (Search(nextNode))
                        return true;
                }
            }
            return false;
        }

        public float GetTraversalCost(Vector2 location, Vector2 parentLocation) {
            return Math.Abs(location.X - parentLocation.X) + Math.Abs(location.Y - parentLocation.Y);
        }

        List<Vector2> GetAdjacentLocations(Vector2 point)
        {
            List<Vector2> result = new List<Vector2>();
            result.Add(new Vector2(point.X - 1, point.Y));
            result.Add(new Vector2(point.X + 1, point.Y));
            result.Add(new Vector2(point.X, point.Y - 1));
            result.Add(new Vector2(point.X, point.Y + 1));
            return result;
        }

        private List<PathfinderNode> GetAdjacentWalkableNodes(PathfinderNode fromPathfinderNode) {
            List<PathfinderNode> walkablePathfindingNodes = new List<PathfinderNode>();
            List<Vector2> nextLocations = GetAdjacentLocations(fromPathfinderNode.Location);

            foreach (var location in nextLocations) {
                int x = (int)location.X;
                int y = (int)location.Y;

                if (x < 0 || x >= MazeGenerator.Instance.Columns || y < 0 || y >= MazeGenerator.Instance.Rows)
                    continue;

                PathfinderNode node = this.nodes[x,y];
                if (!node.IsWalkable)
                    continue;

                if (node.State == PathfinderNode.NodeState.Closed)
                    continue;

                if (node.State == PathfinderNode.NodeState.Open) {
                    float traversalCost = GetTraversalCost(node.Location, node.ParentNode.Location);
                    float gTemp = fromPathfinderNode.G + traversalCost;
                    if (gTemp < node.G) {
                        node.ParentNode = fromPathfinderNode;
                        walkablePathfindingNodes.Add(node);
                    }
                }
                else {
                    node.ParentNode = fromPathfinderNode;
                    node.State = PathfinderNode.NodeState.Open;
                    walkablePathfindingNodes.Add(node);
                }
            }

            return walkablePathfindingNodes;
        }

        public List<Vector2> FindPath(PathfinderNode start) {
            List<Vector2> path = new List<Vector2>();
            bool success = Search(start);
            if (success) {
                PathfinderNode node = this.endNode;
                while (node.ParentNode != null) {
                    path.Add(node.Location);
                    node = node.ParentNode;
                }
                path.Reverse();
            }
            return path;
        }
    }
}

