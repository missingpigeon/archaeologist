﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093.Maze {
    class PathfinderNode
    {
        public enum NodeState
        {
            Untested,
            Open,
            Closed
        }

        private Vector2 _location;
        private bool _isWalkable;
        private float _g;
        private float _h;
        private float _f;
        private NodeState _state;
        private PathfinderNode _parentNode;

        public PathfinderNode(Vector2 location) {
            _location = location;
        }

        public Vector2 Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public bool IsWalkable
        {
            get { return _isWalkable; }
            set { _isWalkable = value; }
        }

        public float G
        {
            get { return _g; }
            set { _g = value; }
        }

        public float H
        {
            get { return _h; }
            set { _h = value; }
        }

        public float F
        {
            get { return G + H; }
            set { _f = value; }
        }

        public NodeState State
        {
            get { return _state; }
            set { _state = value; }
        }

        public PathfinderNode ParentNode
        {
            get { return _parentNode; }
            set { _parentNode = value; }
        }

        public void CalculateCosts()
        {
            G = Math.Abs(Location.X - MazeGenerator.Instance.Entrance.X) + Math.Abs(Location.Y - MazeGenerator.Instance.Entrance.Y);
            H = Math.Abs(MazeGenerator.Instance.Exit.X - Location.X) + Math.Abs(MazeGenerator.Instance.Exit.Y - Location.Y);
        }

        public int CompareTo(PathfinderNode obj)
        {
            if (obj.F > this.F) return -1;
            if (obj.F == this.F) return 0;
            else return 1;
        }
}
}
