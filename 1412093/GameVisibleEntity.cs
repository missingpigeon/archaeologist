﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093 {
    public class GameVisibleEntity : GameEntity
    {
        Sprite2D _model;

        public GameVisibleEntity(Sprite2D model) {
            this._model = model;
        }
        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
            if (_model != null)
                _model.Update(gameTime);
        }
        public virtual void Draw(GameTime gameTime, object handler) {
            if (_model != null)
                _model.Draw(gameTime, handler);
        }

        public float Depth {
            get { return _model.Depth; }
            set { _model.Depth = value; }
        }
        public float X {
            get { return _model.Left; }
        }
        public float Y {
            get { return _model.Top; }
        }
    } 
}
