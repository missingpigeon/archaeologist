﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093 {
    class Global {
        private static Global _instance;
        private Global()
        {
            CellSize = 64;
            IsInGame = false;
            StepsTaken = 0;
            BlankCells = 0;
            PlayerLost = false;
        }
        public static Global Instance {
            get {
                if (_instance == null)
                    _instance = new Global();
                return _instance;
            }
        }

        private int _cellSize;
        public bool PlayerTurn { get; set; }
        public int EnemiesEndedTurn { get; set; }
        public int BlankCells { get; set; }
        public int StepsTaken { get; set; }
        public Camera2D CurrentCamera { get; set; }
        public int CurrentCamIndex { get; set; }
        public Rectangle Viewport { get; internal set; }
        public Map CurrentMap { get; set; }
        public byte PlayerDirection { get; set; }
        public float CarryingWeight { get; set; }
        public bool IsInGame { get; set; }
        public bool PlayerLost { get; set; }

        public int CellSize
        {
            get { return _cellSize; }
            set { _cellSize = value; }
        }
    }
}
