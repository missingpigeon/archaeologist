﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093 {
    class PathUpdaterLinear : PathUpdaterAbtract {
        float _x0 = 0;
        float _y0 = 0;
        float _vx = 0;
        float _vy = 0;
        public PathUpdaterLinear(float x0, float y0, float vx, float vy) {
            _x0 = x0;
            _y0 = y0;
            _vx = vx;
            _vy = vy;
        }

        int t = 0;
        public override void Update(GameTime gameTime) {
            t++;
            float x = _x0 + _vx * t;
            float y = _y0 + _vy * t;
            _position = new Vector2(x, y);

            base.Update(gameTime);
        }
    }
}
