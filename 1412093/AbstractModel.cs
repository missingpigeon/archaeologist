﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093 {
    public abstract class AbstractModel
    {
        public abstract void Update(GameTime gameTime);
        public abstract void Draw(GameTime gameTime, object handler);
    }
}
