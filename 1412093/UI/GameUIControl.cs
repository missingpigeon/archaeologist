﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1412093 {
    public class GameUIControl : AbstractModel
    {
        protected IList<Texture2D> _graphics;

        public const int STATE_NORMAL = 0;
        public const int STATE_HOVER = 1;
        public const int STATE_PRESSED = 2;

        // On-screen coordinates of the control
        protected int _left;
        protected int _top;
        protected int _width;
        protected int _height;

        protected int state = STATE_NORMAL;

        public GameUIControl(IList<Texture2D> controlGraphics, int left, int top, int width, int height)
        {
            this._graphics = controlGraphics;
            this.Left = left;
            this.Top = top;
            this.Width = width;
            this.Height = height;
        }

        public int Left
        {
            get { return _left; }
            set { _left = value; }
        }

        public int Top
        {
            get { return _top; }
            set { _top = value; }
        }

        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public bool isMouseInside(Vector2 mousePos) {
            if (mousePos.X >= this.Left && mousePos.X <= this.Left + this.Width &&
                mousePos.Y >= this.Top && mousePos.Y <= this.Top + this.Height) {
                return true;
            }
            return false;
        }

        public void Select(bool isSelected, Vector2 mousePos)
        {
            if (isSelected && isMouseInside(mousePos)) state = STATE_PRESSED;
            else
            {
                if (isMouseInside(mousePos)) state = STATE_HOVER;
                else state = STATE_NORMAL;
            }
        }

        public override void Update(GameTime gameTime) {

        }

        public override void Draw(GameTime gameTime, object handler) {
           
        }
    }
}
