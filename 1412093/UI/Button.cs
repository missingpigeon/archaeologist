﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _1412093 {
    class Button : GameUIControl
    {

        private SoundEffect _hoverSound;
        private SoundEffect _clickSound;

        private bool _hasBeenPressed = false;
        private bool _hoverSoundHasPlayed = false;
        private SpriteFont _font;
        private string _text = null;


        public Button(IList<Texture2D> textures, int left, int top, int width, int height, string text) : 
            base(textures, left, top, width, height)
        {
            Text = text;
            Font = ResourceManager.Instance.LoadFont("fonts/menu_item");
            _hoverSound = ResourceManager.Instance.LoadSound("sounds/ui/button_hover");
            _clickSound = ResourceManager.Instance.LoadSound("sounds/ui/button_click");
        }


        public delegate void ButtonClickHandler(object sender, ButtonEventArgs e);

        public event ButtonClickHandler Click;

        public SpriteFont Font
        {
            get { return _font; }
            set { _font = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public override void Update(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            Vector2 mousePos = new Vector2(ms.X, ms.Y);
            if (ms.LeftButton == ButtonState.Pressed)
            {
                Select(true, mousePos);
                if (!_hasBeenPressed && isMouseInside(mousePos)) _clickSound.Play();
                _hasBeenPressed = true;
            }
            else Select(false, mousePos);
                
            if (ms.LeftButton == ButtonState.Released && _hasBeenPressed == true)
            {
                if (isMouseInside(mousePos))
                {
                    if (this.Click != null)
                        this.Click(this, new ButtonEventArgs());
                }
                _hasBeenPressed = false;
            }

            if (!isMouseInside(mousePos)) _hoverSoundHasPlayed = false;
        }

        public override void Draw(GameTime gameTime, object handler)
        {
            SpriteBatch spriteBatch = handler as SpriteBatch;
            if (state == STATE_NORMAL)
                spriteBatch.Draw(_graphics[STATE_NORMAL], new Rectangle(Left, Top, Width, Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.7f);
            else if (state == STATE_HOVER)
            {
                if (!_hoverSoundHasPlayed)
                {
                    _hoverSound.Play();
                    _hoverSoundHasPlayed = true;
                }
                spriteBatch.Draw(_graphics[STATE_HOVER], new Rectangle(Left, Top, Width, Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.7f);
            }
            else spriteBatch.Draw(_graphics[STATE_PRESSED], new Rectangle(Left, Top, Width, Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.7f);
            Vector2 textSize = Font.MeasureString(Text);
            int textLeft = (int)(Width - textSize.X) / 2;
            int textTop = (int)(Height - textSize.Y) / 2;
            spriteBatch.DrawString(Font, Text, new Vector2(Left + textLeft, Top + textTop), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.8f);
        }
    }

    class ButtonEventArgs : EventArgs
    {
        
    }

}
