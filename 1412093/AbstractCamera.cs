﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093
{
    class AbstractCamera : GameInvisibleEntity
    {
        protected Matrix _world = Matrix.Identity;
        protected Matrix _view = Matrix.Identity;
        protected Matrix _projection = Matrix.Identity;
        protected PathUpdaterAbtract _pathUpdater;

        public Matrix World
        {
            get { return _world; }

            set { _world = value; }
        }

        public Matrix View
        {
            get { return _view; }

            set { _view = value; }
        }

        public Matrix Projection
        {
            get { return _projection; }

            set { _projection = value; }
        }

        public Matrix WVPMatrix
        {
            get { return World * View * Projection; }
        }

        public Matrix InvWVPMatrix
        {
            get { return Matrix.Invert(WVPMatrix); }
        }

        public PathUpdaterAbtract PathUpdater
        {
            get { return _pathUpdater; }

            set { _pathUpdater = value; }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (_pathUpdater != null)
                _pathUpdater.Update(gameTime);
        }
    }
}