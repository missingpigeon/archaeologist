﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using _1412093.Maze;

namespace _1412093.Sprite {
    class DailyObject : Treasure
    {
        private int _usefulness;

        public DailyObject(Sprite2D sprite, int x , int y) : base(sprite, x ,y)
        {
            this.Sprite = sprite;
            this.CellY = x;
            this.CellX = y;
            this.Usefulness = MazeGenerator.Instance.RNG.Next(0, 101);
            this.Age = MazeGenerator.Instance.RNG.Next(999, 99999);
            this.Weight = (float)(0.5f + MazeGenerator.Instance.RNG.NextDouble() * 8f);
            byte miscSkin = (byte)MazeGenerator.Instance.RNG.Next(1, 6);
            IList<Texture2D> textures = ResourceManager.Instance.LoadSingleTexture("textures/items/misc" + miscSkin.ToString());
            this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
        }

        public int Usefulness
        {
            get { return _usefulness; }
            set { _usefulness = value; }
        }
    }
}
