﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace _1412093 {
    class Raider : NPC
    {
        public Raider(Sprite2D sprite, int x, int y, int hp, int dam) : base(sprite, x, y, hp, dam)
        {
        }

        public bool isMouseInside(Vector2 mousePos) {
            if (mousePos.X >= this.Sprite.Left && mousePos.X <= this.Sprite.Left + this.Sprite.Width &&
                mousePos.Y >= this.Sprite.Top && mousePos.Y <= this.Sprite.Top + this.Sprite.Height) {
                return true;
            }
            return false;
        }

        public void Select(bool isSelected, Vector2 mousePos) {
            if (isSelected && isMouseInside(mousePos)) this.Sprite.State = GameUIControl.STATE_PRESSED;
        }

        public override void Update(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            Vector2 mousePos = new Vector2(ms.X, ms.Y);
            if (ms.LeftButton == ButtonState.Pressed) {
                Select(true, mousePos);
            }
            else Select(false, mousePos);
            Sprite.Update(gameTime);
            if (!isMouseInside(mousePos)) this.Sprite.State = GameUIControl.STATE_NORMAL;
        }
    }
}
