﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _1412093
{
    public class Sprite2D : AbstractModel
    {
        #region Attributes

        // On-screen coordinates of the sprite
        private float _left;

        private float _top;
        private float _width;
        private float _height;
        private float _depth;

        // Frame count for animation sequences
        private int _texture_count;

        // Current texture frame
        private int _current_frame;

        private int state = GameUIControl.STATE_NORMAL;

        // And the textures themselves
        private IList<Texture2D> _textures;

        #endregion

        #region Constructors

        public Sprite2D()
        {
            _current_frame = 0;
        }

        public Sprite2D(IList<Texture2D> textures, float left, float top, float depth)
        {
            _current_frame = 0;
            _left = left;
            _top = top;
            _depth = depth;
            _textures = textures;
            _width = _textures[0].Width;
            _height = _textures[0].Height;
            _texture_count = _textures.Count;
        }

        #endregion

        #region Properties

        public float Left
        {
            get { return _left; }
            set { _left = value; }
        }

        public float Top
        {
            get { return _top; }
            set { _top = value; }
        }

        public float Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public float Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public IList<Texture2D> Textures
        {
            get { return _textures; }
            set
            {
                _textures = value;
                if (_textures == null) throw new Exception("Textures cannot be null!");
                _width = _textures[0].Width;
                _height = _textures[0].Height;
                _texture_count = _textures.Count;
            }
        }

        public int TextureCount
        {
            get { return _texture_count; }
            set { _texture_count = value; }
        }

        public int CurrentFrame
        {
            get { return _current_frame; }
            set { _current_frame = value; }
        }

        public Rectangle BoundingBox
        {
            get
            {
                return new Rectangle(
                    (int) _left,
                    (int) _top,
                    (int) _width,
                    (int) _height);
            }
        }

        public int State
        {
            get { return state; }
            set { state = value; }
        }

        public float Depth
        {
            get { return _depth; }
            set { _depth = value; }
        }

        #endregion

        #region Methods

        public bool collide(Sprite2D target)
        {
            bool result = false;
            if (target.Left >= this.Left && target.Left <= this.Left + this.Width) result = true;
            if (target.Top >= this.Top && target.Top <= this.Top + this.Height) result = true;
            return result;
        }

        public override void Update(GameTime gameTime)
        {
            _current_frame = (_current_frame + 1) % _texture_count;
        }

        public override void Draw(GameTime gameTime, object handler)
        {
            SpriteBatch spriteBatch = handler as SpriteBatch;
            if (State == GameUIControl.STATE_PRESSED)
                spriteBatch.Draw(_textures[_current_frame], new Vector2(_left, _top), new Rectangle(0, 0, (int)_width, (int)_height), 
                    Color.Yellow, 0f, Vector2.Zero, 1f, SpriteEffects.None, Depth);
            else
                spriteBatch.Draw(_textures[_current_frame], new Vector2(_left, _top), new Rectangle(0, 0, (int)_width, (int)_height),
                    Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, Depth);
        }

        #endregion
    }
}