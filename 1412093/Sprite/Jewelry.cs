﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using _1412093.Maze;

namespace _1412093.Sprite {
    class Jewelry : Treasure
    {
        private int _material; // 0 = gold, 1 = silver, 2 = copper
        private int _condition;

        public Jewelry(Sprite2D sprite, int x, int y) : base(sprite, x, y)
        {
            this.Sprite = sprite;
            this.CellX = x;
            this.CellY = y;
            this.Age = MazeGenerator.Instance.RNG.Next(999, 99999);
            this.Weight = (float)(0.2f + MazeGenerator.Instance.RNG.NextDouble() * 5f);
            this.Material = MazeGenerator.Instance.RNG.Next(0, 4);
            IList<Texture2D> textures = new List<Texture2D>();
            switch (this.Material)
            {
                case 0:
                    Condition = (int)(1000 - (1000 / (Age * 0.0001) * 100));
                    textures = ResourceManager.Instance.LoadSingleTexture("textures/items/gold");
                    this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
                    break;
                case 1:
                    Condition = (int)(800 - (800 / (Age * 0.0001) * 100));
                    textures = ResourceManager.Instance.LoadSingleTexture("textures/items/silver");
                    this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
                    break;
                case 2:
                    Condition = (int)(300 - (300 / (Age * 0.0001) * 100));
                    textures = ResourceManager.Instance.LoadSingleTexture("textures/items/copper");
                    this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
                    break;
                default:
                    Condition = (int)(300 - (300 / (Age * 0.0001) * 100));
                    textures = ResourceManager.Instance.LoadSingleTexture("textures/items/copper");
                    this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
                    break;
            }
        }

        public int Material
        {
            get { return _material; }
            set { _material = value; }
        }

        public int Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }
    }
}
