﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093 {
    public class Map : GameVisibleEntity {
        public Map(Sprite2D sprite2D) : base(sprite2D) {

        }

        public Vector3 World2Screen(Vector3 coord) {
            return Vector3.Transform(coord, Global.Instance.CurrentCamera.WVPMatrix);
        }
        public Vector3 Screen2World(Vector3 coord) {
            return Vector3.Transform(coord, Global.Instance.CurrentCamera.InvWVPMatrix);
        }
    }
}
