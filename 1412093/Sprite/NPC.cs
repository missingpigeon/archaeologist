﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using _1412093.Maze;

namespace _1412093 {
    public abstract class NPC : GameVisibleEntity
    {
        // Basic stats for all NPCs
        private int _hitpoints;
        private int _attackDamage;
        // NPC has detected player or not
        private bool _aggro;
        // A Sprite2D to visualize the NPC in-game
        private Sprite2D _sprite;
        // Map coordinates of the NPC
        private int _cellX;
        private int _cellY;
        // In-game representation status
        private bool _isMoving;
        private Vector2 _destination;

        #region Constructors
        public NPC(Sprite2D sprite, int x, int y, int hp, int dam) : base(sprite) {
            Hitpoints = hp;
            AttackDamage = dam;
            _sprite = sprite;
            _sprite.Width = Global.Instance.CellSize;
            _sprite.Height = Global.Instance.CellSize;
            _cellX = x;
            _cellY = y;
            Aggro = false;
            _destination = Vector2.Zero;
            IsMoving = false;
        }
        #endregion

        #region Properties
        public int Hitpoints {
            get { return _hitpoints; }
            set { _hitpoints = value; }
        }

        public int AttackDamage
        {
            get { return _attackDamage; }
            set { _attackDamage = value; }
        }

        public Sprite2D Sprite
        {
            get { return _sprite; }
            set { _sprite = value; }
        }

        public int CellX
        {
            get { return _cellX; }
            set { _cellX = value; }
        }

        public int CellY
        {
            get { return _cellY; }
            set { _cellY = value; }
        }

        public bool Aggro
        {
            get { return _aggro; }
            set { _aggro = value; }
        }

        public bool IsMoving
        {
            get { return _isMoving; }
            set { _isMoving = value; }
        }

        #endregion

        #region Methods
        public override void Draw(GameTime gameTime, object spriteBatch)
        {
            Sprite.Draw(gameTime, spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            if (IsMoving) MoveToCell((int)_destination.X, (int)_destination.Y);
            Sprite.Update(gameTime);
        }

        // Pathfinding
        public virtual Cell[] findPath()
        {
            return new Cell[1];
        }

        // NPC vision
        public virtual bool Detect(Cell[,] mazeState, Vector2 playerPos)
        {
            return false;
        }

        public virtual void RandomizeDestination()
        {
            List<Vector2> WalkableAdjacentCells = new List<Vector2>();
            if (MazeGenerator.Instance.Cells[CellX -1, CellY].State != 1) WalkableAdjacentCells.Add(new Vector2(CellX - 1, CellY));
            if (MazeGenerator.Instance.Cells[CellX + 1, CellY].State != 1) WalkableAdjacentCells.Add(new Vector2(CellX + 1, CellY));
            if (MazeGenerator.Instance.Cells[CellX, CellY - 1].State != 1) WalkableAdjacentCells.Add(new Vector2(CellX, CellY - 1));
            if (MazeGenerator.Instance.Cells[CellX, CellY + 1].State != 1) WalkableAdjacentCells.Add(new Vector2(CellX, CellY + 1));
            int index = MazeGenerator.Instance.RNG.Next(WalkableAdjacentCells.Count);
            _destination = WalkableAdjacentCells[index];
            IsMoving = true;
        }

        public void MoveToCell(int x, int y)
        {
            if (Sprite.Top < x * Global.Instance.CellSize) Sprite.Top += 4;
            if (Sprite.Left < y * Global.Instance.CellSize) Sprite.Left += 4;
            if (Sprite.Top > x * Global.Instance.CellSize) Sprite.Top -= 4;
            if (Sprite.Left > y * Global.Instance.CellSize) Sprite.Left -= 4;
            if ((int)Sprite.Top == x * Global.Instance.CellSize && (int)Sprite.Left == y * Global.Instance.CellSize) {
                CellX = x;
                CellY = y;
                Global.Instance.EnemiesEndedTurn++;
                Debug.WriteLine("Enemies ended turn: " + Global.Instance.EnemiesEndedTurn);
                this.IsMoving = false;
            }
        }

        public void MoveLeft() {
            if (CellY - 1 >= 0 && MazeGenerator.Instance.Cells[CellX, CellY - 1].State == 0)
                MoveToCell(CellX, CellY - 1);
            else Global.Instance.PlayerDirection = 0;
        }

        public void MoveUp() {
            if (CellX - 1 >= 0 && MazeGenerator.Instance.Cells[CellX - 1, CellY].State == 0)
                MoveToCell(CellX - 1, CellY);
            else Global.Instance.PlayerDirection = 0;
        }

        public void MoveRight() {
            if (CellY + 1 <= MazeGenerator.Instance.Columns - 1 &&
                MazeGenerator.Instance.Cells[CellX, CellY + 1].State == 0)
                MoveToCell(CellX, CellY + 1);
            else Global.Instance.PlayerDirection = 0;
        }

        public void MoveDown() {
            if (CellX + 1 <= MazeGenerator.Instance.Rows - 1 &&
                MazeGenerator.Instance.Cells[CellX + 1, CellY].State == 0)
                MoveToCell(CellX + 1, CellY);
            else Global.Instance.PlayerDirection = 0;
        }

        // Basic attack of the NPC, will probably expand this later
        // to include and differentiate melee, ranged, ...
        public void Attack(NPC targetNpc)
        {
           targetNpc.TakeDamage(this.AttackDamage);
        }

        public void TakeDamage(int damage)
        {
            this.Hitpoints -= (int)damage;
        }
        #endregion
    }
}
