﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using _1412093.Maze;

namespace _1412093.Sprite
{
    class SquareMap : Map
    {
        Sprite2D[,] _mapCell;
        int _row;
        int _col;
        int _cellW;
        int _cellH;
        private Cell[,] _maze;

        public SquareMap(Cell[,] maze, int row, int col, int cellWidth, int cellHeight, float depth = 0f) :
            base(null)
        {
            _maze = maze;
            _mapCell = new Sprite2D[row, col];
            _col = col;
            _row = row;
            _cellW = cellWidth;
            _cellH = cellHeight;

            // Load textures to mapcells
            for (int i = 0; i < _row; i++)
            for (int j = 0; j < _col; j++)
            {
                IList<Texture2D> cellTexture = new List<Texture2D>();
                if (_maze[i, j].State == 0)
                {
                    if (i == MazeGenerator.Instance.Entrance.X && j == MazeGenerator.Instance.Entrance.Y)
                        cellTexture = ResourceManager.Instance.LoadSingleTexture("textures/map/map_entrance");
                    else if (i == MazeGenerator.Instance.Exit.X && j == MazeGenerator.Instance.Exit.Y)
                        cellTexture = ResourceManager.Instance.LoadSingleTexture("textures/map/map_exit");
                    else cellTexture = ResourceManager.Instance.LoadSingleTexture("textures/map/map_path");
                }
                else if (_maze[i, j].State == 1)
                    cellTexture = ResourceManager.Instance.LoadSingleTexture("textures/map/map_wall");

                _mapCell[i, j] = new Sprite2D(
                    cellTexture,
                    j * _cellW,
                    i * _cellH,
                    depth
                );
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            for (int i = 0; i < _row; i++)
            for (int j = 0; j < _col; j++)
                _mapCell[i, j].Update(gameTime);
        }

        public override void Draw(GameTime gameTime, object handler)
        {
            base.Draw(gameTime, handler);
            Vector3 worldPos = Screen2World(new Vector3(0, 0, 0));
            for (int i = 0; i < _row; i++)
            for (int j = 0; j < _col; j++)
                _mapCell[i, j].Draw(gameTime, handler);
        }
    }
}