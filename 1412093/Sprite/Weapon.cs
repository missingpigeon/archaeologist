﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using _1412093.Maze;

namespace _1412093.Sprite {
    class Weapon : Treasure
    {
        private int _damage;

        public Weapon(Sprite2D sprite, int x, int y) : base(sprite, x, y)
        {
            this.Sprite = sprite;
            this.CellX = x;
            this.CellY = y;
            this.Damage = MazeGenerator.Instance.RNG.Next(10, 101);
            this.Age = MazeGenerator.Instance.RNG.Next(999, 99999);
            this.Weight = (float)(0.5f + MazeGenerator.Instance.RNG.NextDouble() * 15f);
            byte weaponSkin = (byte)MazeGenerator.Instance.RNG.Next(1, 6);
            IList<Texture2D> textures = ResourceManager.Instance.LoadSingleTexture("textures/items/weapon" + weaponSkin.ToString());
            this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
        }

        public int Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }
    }
}
