﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093.Sprite {
    public abstract class Treasure : GameVisibleEntity {
        private Sprite2D _sprite;
        // Map coordinates of the trasure
        private int _cellX;
        private int _cellY;
        private int type;

        // Age of the treasure, in years
        private int _age;

        // Weight of the treasure, in kilograms
        private float _weight;


        public Treasure(Sprite2D sprite, int x, int y) : base(sprite)
        {
            _sprite = sprite;
            _sprite.Width = Global.Instance.CellSize;
            _sprite.Height = Global.Instance.CellSize;
            this.CellY = x;
            this.CellX = y;
        }

        public Sprite2D Sprite
        {
            get { return _sprite; }
            set { _sprite = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public float Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public int CellX
        {
            get { return _cellX; }
            set { _cellX = value; }
        }

        public int CellY
        {
            get { return _cellY; }
            set { _cellY = value; }
        }

        #region Methods
        public override void Draw(GameTime gameTime, object spriteBatch) {
            Sprite.Draw(gameTime, spriteBatch);
        }

        public void MoveToCell(int x, int y) {
            if (Sprite.Top < x * Global.Instance.CellSize) Sprite.Top += 4;
            if (Sprite.Left < y * Global.Instance.CellSize) Sprite.Left += 4;
            if (Sprite.Top > x * Global.Instance.CellSize) Sprite.Top -= 4;
            if (Sprite.Left > y * Global.Instance.CellSize) Sprite.Left -= 4;
        }

        public bool PickUp(int playerX, int playerY)
        {
            if (CellX == playerX && CellY == playerY) return true;
            else return false;
        }

        public override void Update(GameTime gameTime)
        {
            Sprite.Update(gameTime);
        }
        #endregion
    }
}
