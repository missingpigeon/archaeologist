﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using _1412093.Maze;

namespace _1412093.Sprite
{
    public class Player : GameVisibleEntity
    {
        private Sprite2D _sprite;
        private int _cellX;
        private int _cellY;

        public Sprite2D Sprite
        {
            get { return _sprite; }
            set { _sprite = value; }
        }

        public int CellX
        {
            get { return _cellX; }
            set { _cellX = value; }
        }

        public int CellY
        {
            get { return _cellY; }
            set { _cellY = value; }
        }

        public Player(Sprite2D sprite, int x, int y) : base(sprite)
        {
            _sprite = sprite;
            CellX = x;
            CellY = y;
        }

        public void MoveToCell(int x, int y)
        {
            if (Sprite.Top < x * Global.Instance.CellSize) Sprite.Top += 4;
            if (Sprite.Left < y * Global.Instance.CellSize) Sprite.Left += 4;
            if (Sprite.Top > x * Global.Instance.CellSize) Sprite.Top -= 4;
            if (Sprite.Left > y * Global.Instance.CellSize) Sprite.Left -= 4;
            if ((int)Sprite.Left == y * Global.Instance.CellSize && (int)Sprite.Top == x * Global.Instance.CellSize)
            {
                CellX = x;
                CellY = y;
                Global.Instance.StepsTaken++;
                Global.Instance.PlayerDirection = 0;
                Global.Instance.PlayerTurn = false;
            }
        }

        public void MoveLeft()
        {
            if (CellY - 1 >= 0 && MazeGenerator.Instance.Cells[CellX, CellY - 1].State == 0)
                MoveToCell(CellX, CellY - 1);
            else Global.Instance.PlayerDirection = 0;
        }

        public void MoveUp()
        {
            if (CellX - 1 >= 0 && MazeGenerator.Instance.Cells[CellX - 1, CellY].State == 0)
                MoveToCell(CellX - 1, CellY);
            else Global.Instance.PlayerDirection = 0;
        }

        public void MoveRight()
        {
            if (CellY + 1 <= MazeGenerator.Instance.Columns - 1 &&
                MazeGenerator.Instance.Cells[CellX, CellY + 1].State == 0)
                MoveToCell(CellX, CellY + 1);
            else Global.Instance.PlayerDirection = 0;
        }

        public void MoveDown()
        {
            if (CellX + 1 <= MazeGenerator.Instance.Rows - 1 &&
                MazeGenerator.Instance.Cells[CellX + 1, CellY].State == 0)
                MoveToCell(CellX + 1, CellY);
            else Global.Instance.PlayerDirection = 0;
        }

        public override void Draw(GameTime gameTime, object spriteBatch)
        {
            Sprite.Draw(gameTime, spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            Sprite.Update(gameTime);
        }
    }
}