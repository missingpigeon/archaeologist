﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace _1412093 {
    class ResourceManager
    {

        private Game _game = null;
        private static ResourceManager _instance = null;

        private ResourceManager()
        {
        }

        public static ResourceManager Instance
        {
            get
            {
                if (_instance == null) _instance = new ResourceManager();
                return _instance;
            }
        }

        public void Initialize(Game game)
        {
            _game = game;
        }

        public IList<Texture2D> LoadSingleTexture(string path) {
            List<Texture2D> textures = new List<Texture2D>();
            textures.Add(_game.Content.Load<Texture2D>(path));
            return textures;
        }

        public IList<Texture2D> LoadTextures(string[] paths)
        {
            List<Texture2D> textures = new List<Texture2D>();
            for (int i = 0; i < paths.Length; i++)
            {
                textures.Add(_game.Content.Load<Texture2D>(paths[i]));
            }
            return textures;
        }

        public SpriteFont LoadFont(string fontName)
        {
            return _game.Content.Load<SpriteFont>(fontName);
        }

        public Song LoadMusic(string audioName)
        {
            return _game.Content.Load<Song>(audioName);
        }

        public SoundEffect LoadSound(string audioName) {
            return _game.Content.Load<SoundEffect>(audioName);
        }
    }
}
