﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _1412093 {
    class Constants
    {
        public const string config = "config.json";
        public static string APP_PATH = String.Empty;

        static Constants()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var exePath = new UriBuilder(codeBase).Path;
            APP_PATH = Path.GetDirectoryName(exePath);
        }
    }
}
