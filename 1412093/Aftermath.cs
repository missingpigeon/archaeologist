﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using _1412093.Maze;
using _1412093.Sprite;

namespace _1412093
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Aftermath : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Vector2 position = Vector2.Zero;

        private IList<GameVisibleEntity> _entities;

        // Separation of game entities and UI controls is probably a bad idea
        // but personally it makes sense to me
        private IList<GameUIControl> _controls;

        private IList<GameInvisibleEntity> _inv_entities;

        private IList<Camera2D> _cameras;

        private IList<NPC> _enemies;

        private IList<Treasure> _treasures;

        private int _current_camera = 0;

        private MouseDragDrop dragdrop = null;

        private Player _player;

        private SpriteFont _hudFont;

        private Button returnToMenuButton;

        private Button continueGameButton;

        private Song menuMusic;

        private Song inGameMusic;

        private SoundEffect playerFootstep;

        private SoundEffect enemyTurn;

        private SoundEffect itemPickup;

        private SoundEffect ded;

        private bool pickupSoundPlayed;

        private bool playFootstepSound;

        private bool footstepSoundPlayed;

        public Aftermath()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            _entities = new List<GameVisibleEntity>();
            _controls = new List<GameUIControl>();
            _cameras = new List<Camera2D>();
            _inv_entities = new List<GameInvisibleEntity>();
            _enemies = new List<NPC>();
            _treasures = new List<Treasure>();
            graphics.PreferredBackBufferWidth = 672;
            graphics.PreferredBackBufferHeight = 672;
            graphics.IsFullScreen = false;
            this.IsMouseVisible = true;
            graphics.ApplyChanges();

            pickupSoundPlayed = true;
            playFootstepSound = false;
            footstepSoundPlayed = true;


            ResourceManager.Instance.Initialize(this);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //_entities.Add(CreateRaider());
            menuMusic = ResourceManager.Instance.LoadMusic("sounds/music/menu");
            inGameMusic = ResourceManager.Instance.LoadMusic("sounds/music/ingame");
            playerFootstep = ResourceManager.Instance.LoadSound("sounds/player/footstep");
            enemyTurn = ResourceManager.Instance.LoadSound("sounds/npc/enemy");
            itemPickup = ResourceManager.Instance.LoadSound("sounds/player/pickup");
            ded = ResourceManager.Instance.LoadSound("sounds/player/die");
            var mainCamera = new Camera2D(1f, 1f, 0f, 0f, 0f);
            _cameras.Add(mainCamera);

            Global.Instance.Viewport = this.Window.ClientBounds;
            Global.Instance.CurrentCamIndex = 0;
            Global.Instance.CurrentCamera = _cameras[Global.Instance.CurrentCamIndex];

            dragdrop = new MouseDragDrop(_cameras[Global.Instance.CurrentCamIndex]);
            _inv_entities.Add(mainCamera);
            _inv_entities.Add(dragdrop);


            ShowMainMenu();
        }

        // I swear I'll fix the hardcoded coordinates later
        private void ShowMainMenu()
        {
            Global.Instance.IsInGame = false;
            Global.Instance.StepsTaken = 0;
            Global.Instance.BlankCells = 0;
            _entities.Clear();
            _controls.Clear();
            _enemies.Clear();
            _treasures.Clear();
            returnToMenuButton = null;
            _entities.Add(CreateMenuBackground());
            var newGameButton = CreateMenuButton(176, 220, 313, 47, "NEW GAME");
            newGameButton.Click += (object sender, ButtonEventArgs e) => { NewGame(); };
            var loadGameButton = CreateMenuButton(176, 280, 313, 47, "LOAD GAME");
            loadGameButton.Click += (object sender, ButtonEventArgs e) => { LoadGame(); };
            var settingsButton = CreateMenuButton(176, 340, 313, 47, "SETTINGS");
            settingsButton.Click += (object sender, ButtonEventArgs e) => { Settings(); };
            var exitGameButton = CreateMenuButton(176, 400, 313, 47, "EXIT TO DESKTOP");
            exitGameButton.Click += (object sender, ButtonEventArgs e) => { ExitGame(); };
            _controls.Add(newGameButton);
            _controls.Add(loadGameButton);
            _controls.Add(settingsButton);
            _controls.Add(exitGameButton);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(menuMusic);
        }

        private void NewGame()
        {
            Global.Instance.PlayerLost = false;
            _entities.Clear();
            _controls.Clear();
            MazeGenerator.Instance.Columns = 21;
            MazeGenerator.Instance.Rows = 21;
            MazeGenerator.Instance.generateMaze();
            MazeGenerator.Instance.outputToConsole();
            Map map = new SquareMap(MazeGenerator.Instance.Cells, 21, 21, Global.Instance.CellSize,
                Global.Instance.CellSize);
            _entities.Add(map);
            Global.Instance.CurrentMap = map;
            Global.Instance.PlayerDirection = 0;
            Global.Instance.CarryingWeight = 0;
            for (int i = 0; i < MazeGenerator.Instance.Rows; i++)
            {
                for (int j = 0; j < MazeGenerator.Instance.Columns; j++)
                {
                    if (MazeGenerator.Instance.Cells[i, j].State != 1) Global.Instance.BlankCells++;
                    if (MazeGenerator.Instance.Cells[i, j].HasSpawn == false)
                    {
                        if (MazeGenerator.Instance.Cells[i, j].Content == 1)
                        {
                            var newMonster = RandomizeMonster(i, j);
                            _enemies.Add(newMonster);
                            Debug.WriteLine("Spawned monster: " + "[" + newMonster.CellX.ToString() + ", " +
                                            newMonster.CellY.ToString() + "]: " + newMonster.Sprite.Left.ToString() +
                                            ", " + newMonster.Sprite.Top.ToString());
                        }
                        else if (MazeGenerator.Instance.Cells[i, j].Content == 2)
                        {
                            var newTreasure = RandomizeTreasure(i, j);
                            _treasures.Add(newTreasure);
                            Debug.WriteLine("Spawned treasure: " + "[" + newTreasure.CellX.ToString() + ", " +
                                            newTreasure.CellY.ToString() + "]: " + newTreasure.Sprite.Left.ToString() +
                                            ", " + newTreasure.Sprite.Top.ToString());
                        }
                        MazeGenerator.Instance.Cells[i, j].HasSpawn = true;
                    }
                }
            }

            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/npcs/player"));
            _player = new Player(
                new Sprite2D(textures, Global.Instance.CellSize * MazeGenerator.Instance.Entrance.Y,
                    Global.Instance.CellSize * MazeGenerator.Instance.Entrance.X,
                    0.2f), MazeGenerator.Instance.Entrance.X, MazeGenerator.Instance.Entrance.Y);
            Debug.WriteLine("Player spawned at [" + _player.CellX + ", " + _player.CellY + "]" + ": " +
                            _player.Sprite.Left + ", " + _player.Sprite.Top);

            _hudFont = ResourceManager.Instance.LoadFont("fonts/menu_item");

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(inGameMusic);

            Global.Instance.IsInGame = true;
            Global.Instance.EnemiesEndedTurn = 0;
            Global.Instance.PlayerTurn = true;

            returnToMenuButton = CreateButton(460, 12, 190, 50, "Back to main menu");
            returnToMenuButton.Click += (object sender, ButtonEventArgs e) => { ShowMainMenu(); };

            pickupSoundPlayed = true;
            playFootstepSound = false;
            footstepSoundPlayed = false;
        }

        private void LoadGame()
        {
            _entities.Clear();
            _controls.Clear();
            _entities.Add(CreateMenuBackground());
            var cancelButton = CreateButton(944, 594, 220, 70, "Cancel");
            cancelButton.Click += (object sender, ButtonEventArgs e) => { ShowMainMenu(); };
            var loadButton = CreateButton(720, 594, 220, 70, "Load");
            loadButton.Click += (object sender, ButtonEventArgs e) => { ShowMainMenu(); };
            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/ui/load_dialog"));
            _entities.Add(new GameVisibleEntity(new Sprite2D(textures, 0, 0, 0)));
            _controls.Add(loadButton);
            _controls.Add(cancelButton);
        }

        private void Settings()
        {
        }

        private void ExitGame()
        {
            Exit();
        }

        private NPC RandomizeMonster(int x, int y)
        {
            byte monsterType = (byte) MazeGenerator.Instance.RNG.Next(0, 3);
            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/generic/placeholder"));
            switch (monsterType)
            {
                case 0:
                    return new Mummy(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.2f), x, y,
                        100, 100);
                    break;
                case 1:
                    return new Scorpion(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.2f), x, y,
                        100, 100);
                    break;
                case 2:
                    return new Jiangshi(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.2f), x, y,
                        100, 100);
                    break;
                default:
                    return new Mummy(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.2f), x, y,
                        100, 100);
                    break;
            }
        }

        private Treasure RandomizeTreasure(int x, int y)
        {
            byte treasureType = (byte) MazeGenerator.Instance.RNG.Next(0, 3);
            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/generic/placeholder"));
            switch (treasureType)
            {
                case 0:
                    return new Jewelry(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.15f), x,
                        y);
                    break;
                case 1:
                    return new Weapon(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.15f), x,
                        y);
                    break;
                case 2:
                    return new Figurine(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.15f), x,
                        y);
                    break;
                case 3:
                    return new DailyObject(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.15f), x,
                        y);
                    break;
                default:
                    return new Jewelry(
                        new Sprite2D(textures, Global.Instance.CellSize * y, Global.Instance.CellSize * x, 0.15f), x,
                        y);
                    break;
            }
        }

        private GameVisibleEntity CreateMenuBackground()
        {
            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/ui/menu_background"));
            return new GameVisibleEntity(new Sprite2D(textures, 0, 0, 0.1f));
        }

        private GameVisibleEntity CreateMenuTitle()
        {
            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/ui/menu_logo"));
            return new GameVisibleEntity(new Sprite2D(textures, 316, 70, 0.11f));
        }

        private Button CreateButton(int left, int top, int width, int height, string text)
        {
            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/ui/button"));
            textures.Add(this.Content.Load<Texture2D>("textures/ui/button_hover"));
            textures.Add(this.Content.Load<Texture2D>("textures/ui/button_pressed"));
            return new Button(textures, left, top, width, height, text);
        }

        private Button CreateMenuButton(int left, int top, int width, int height, string text)
        {
            IList<Texture2D> textures = new List<Texture2D>();
            textures.Add(this.Content.Load<Texture2D>("textures/ui/menu_button"));
            textures.Add(this.Content.Load<Texture2D>("textures/ui/menu_button_select"));
            textures.Add(this.Content.Load<Texture2D>("textures/ui/menu_button_pressed"));
            return new Button(textures, left, top, width, height, text);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            MouseState ms = Mouse.GetState();
            if (ms.ScrollWheelValue != 0)
            {
                float scale = ms.ScrollWheelValue * 0.0005f;
                Global.Instance.CurrentCamera.ZoomOnDemand(new Vector3(ms.X, ms.Y, 0), scale);
            }

            KeyboardState kb = Keyboard.GetState();
            if (Global.Instance.IsInGame && Global.Instance.PlayerTurn && Global.Instance.PlayerLost == false)
            {
                if (kb.IsKeyDown(Keys.W) || kb.IsKeyDown(Keys.Up))
                {
                    if (Global.Instance.PlayerDirection == 0) Global.Instance.PlayerDirection = 2;
                    playFootstepSound = true;
                    if (playFootstepSound == true && footstepSoundPlayed == false) {
                        playerFootstep.Play();
                        playFootstepSound = false;
                        footstepSoundPlayed = true;
                    }
                }

                if (kb.IsKeyDown(Keys.S) || kb.IsKeyDown(Keys.Down))
                {
                    if (Global.Instance.PlayerDirection == 0) Global.Instance.PlayerDirection = 4;
                    playFootstepSound = true;
                    if (playFootstepSound == true && footstepSoundPlayed == false) {
                        playerFootstep.Play();
                        playFootstepSound = false;
                        footstepSoundPlayed = true;
                    }
                }

                if (kb.IsKeyDown(Keys.A) || kb.IsKeyDown(Keys.Left))
                {
                    if (Global.Instance.PlayerDirection == 0) Global.Instance.PlayerDirection = 1;
                    playFootstepSound = true;
                    if (playFootstepSound == true && footstepSoundPlayed == false) {
                        playerFootstep.Play();
                        playFootstepSound = false;
                        footstepSoundPlayed = true;
                    }
                }

                if (kb.IsKeyDown(Keys.D) || kb.IsKeyDown(Keys.Right))
                {
                    if (Global.Instance.PlayerDirection == 0) Global.Instance.PlayerDirection = 3;
                    playFootstepSound = true;
                    if (playFootstepSound == true && footstepSoundPlayed == false) {
                        playerFootstep.Play();
                        playFootstepSound = false;
                        footstepSoundPlayed = true;
                    }
                }

                switch (Global.Instance.PlayerDirection)
                {
                    case 1:
                        _player.MoveLeft();
                        break;
                    case 2:
                        _player.MoveUp();
                        break;
                    case 3:
                        _player.MoveRight();
                        break;
                    case 4:
                        _player.MoveDown();
                        break;
                    default:
                        break;
                }
            }
            if (returnToMenuButton != null) returnToMenuButton.Update(gameTime);
            for (int i = 0; i < _entities.Count; i++)
            {
                _entities[i].Update(gameTime);
            }
            for (int i = 0; i < _controls.Count; i++)
            {
                _controls[i].Update(gameTime);
            }
            for (int i = 0; i < _inv_entities.Count; i++)
            {
                _inv_entities[i].Update(gameTime);
            }
            for (int i = 0; i < _enemies.Count; i++)
            {
                if (_enemies[i].CellX == _player.CellX && _enemies[i].CellY == _player.CellY)
                {
                    if (!Global.Instance.PlayerLost) ded.Play();
                    Global.Instance.PlayerLost = true;
                }
                    
                if (Global.Instance.PlayerTurn == false && _enemies[i].IsMoving == false)
                    _enemies[i].RandomizeDestination();
                
                _enemies[i].Update(gameTime);
            }
            for (int i = 0; i < _treasures.Count; i++)
            {
                _treasures[i].Update(gameTime);
                if (_treasures[i].PickUp(_player.CellX, _player.CellY))
                {
                    if (Global.Instance.CarryingWeight + _treasures[i].Weight <= 50)
                    {
                        pickupSoundPlayed = false;
                        Global.Instance.CarryingWeight += _treasures[i].Weight;
                        _treasures.RemoveAt(i);
                    }
                }
            }
            if (_player != null) _player.Update(gameTime);
            if (Global.Instance.EnemiesEndedTurn == _enemies.Count)
            {
                if (!Global.Instance.PlayerTurn && _enemies.Count > 0) enemyTurn.Play();
                footstepSoundPlayed = false;
                Global.Instance.PlayerTurn = true;
                Global.Instance.EnemiesEndedTurn = 0;
            }

            if (!pickupSoundPlayed)
            {
                itemPickup.Play();
                pickupSoundPlayed = true;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SandyBrown);
            if (Global.Instance.IsInGame)
            {
                spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null,
                    _cameras[_current_camera].WVPMatrix);
                for (int i = 0; i < _entities.Count; i++)
                {
                    _entities[i].Draw(gameTime, spriteBatch);
                }
                for (int i = 0; i < _controls.Count; i++)
                {
                    _controls[i].Draw(gameTime, spriteBatch);
                }
                for (int i = 0; i < _enemies.Count; i++)
                {
                    _enemies[i].Draw(gameTime, spriteBatch);
                }
                for (int i = 0; i < _treasures.Count; i++)
                {
                    _treasures[i].Draw(gameTime, spriteBatch);
                }
                if (_player != null) _player.Draw(gameTime, spriteBatch);
                spriteBatch.End();
                spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
                spriteBatch.DrawString(_hudFont,
                    "Inventory: " + (Global.Instance.CarryingWeight > 0
                        ? Global.Instance.CarryingWeight.ToString("0.00")
                        : "0") + "/50",
                    new Vector2(20, 20), Color.White);
                if (_player.CellX == MazeGenerator.Instance.Exit.X && _player.CellY == MazeGenerator.Instance.Exit.Y &&
                    Global.Instance.CarryingWeight > 0)
                {
                    spriteBatch.DrawString(_hudFont, "Congratulations, you escaped the maze!", new Vector2(200, 200),
                        Color.White);
                    spriteBatch.DrawString(_hudFont,
                        "You took " + Global.Instance.StepsTaken + " steps to reach the exit.", new Vector2(200, 250),
                        Color.White);
                    spriteBatch.DrawString(_hudFont,
                        "Your score is " +
                        MathHelper.Max(3 * Global.Instance.BlankCells - Global.Instance.StepsTaken, 0) + ".",
                        new Vector2(200, 300),
                        Color.White);
                    returnToMenuButton.Left = 241;
                    returnToMenuButton.Top = 380;
                }
                if (Global.Instance.PlayerLost)
                {
                    spriteBatch.DrawString(_hudFont, "You died. Unsurprisingly.", new Vector2(240, 300),
                        Color.White);
                    returnToMenuButton.Left = 241;
                    returnToMenuButton.Top = 380;
                }
                if (returnToMenuButton != null) returnToMenuButton.Draw(gameTime, spriteBatch);
                spriteBatch.End();
            }
            else
            {
                spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
                for (int i = 0; i < _entities.Count; i++)
                {
                    _entities[i].Draw(gameTime, spriteBatch);
                }
                for (int i = 0; i < _controls.Count; i++)
                {
                    _controls[i].Draw(gameTime, spriteBatch);
                }
                spriteBatch.End();
            }
            base.Draw(gameTime);
        }
    }
}