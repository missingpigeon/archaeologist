﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093 {
    class PathUpdaterAbtract : GameInvisibleEntity {
        protected Vector2 _position;

        public Vector2 Position {
            get {
                return _position;
            }
            set {
                _position = value;
            }
        }
    }
}
