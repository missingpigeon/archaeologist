﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace _1412093 {
    class MouseDragDrop : GameInvisibleEntity {
        Camera2D _cam;
        MouseEvent mEvent;
        bool bDrag = false;
        bool bFloat = false;

        public MouseDragDrop(Camera2D camera) {
            _cam = camera;
            mEvent = new MouseEvent();
            mEvent.OnLMouseDown += mEvent_OnLMouseDown;
            mEvent.OnLMouseUp += mEvent_OnLMouseUp;
        }

        private void mEvent_OnLMouseDown(object sender, MouseEventArgs e) {
            bDrag = true;
            bFloat = false;
        }

        Vector2 v = Vector2.Zero;
        Vector2 a = Vector2.Zero;
        float t = 0;
        private void mEvent_OnLMouseUp(object sender, MouseEventArgs e) {
            bDrag = false;
            bFloat = true;
            v = mEvent.GetDiff();
            a.X = 0.1f * v.X;
            a.Y = 0.1f * v.Y;

            _cam.Move(v.X, v.Y);
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
            mEvent.Update(gameTime);

            if (bDrag) {
                Vector2 delta = mEvent.GetDiff();
                _cam.Move(delta.X, delta.Y);
            }
            else if (bFloat) {
                v.X -= a.X;
                v.Y -= a.Y;
                if (v.X < 0.1f && v.Y < 0.1f) {
                    bFloat = false;
                    v.X = 0f;
                    v.Y = 0f;
                    a.X = 0f;
                    a.Y = 0f;
                }

                _cam.Move(v.X, v.Y);
            }
        }
    }
}
